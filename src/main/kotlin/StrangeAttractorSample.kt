import processing.core.PApplet

data class Vector(val x: Float = 0f, val y: Float = 0f, val z: Float = 0f) {
    fun scale(scalar: Float): Vector = Vector(
        x = this.x * scalar,
        y = this.y * scalar,
        z = this.z * scalar
    )
}

class StrangeAttractorSample : PApplet() {

    private val scale = 1f

    private val vectors = listOf(
        Vector(20f, 70f).scale(scale),
        Vector(780f, 70f).scale(scale),
        Vector(400f, 730f).scale(scale)
    )

    override fun settings() {
        size(800 * scale.toInt(), 800 * scale.toInt(), FX2D)
    }

    override fun setup() {
        colorMode(RGB)
        background(255)

        var startVector = Vector(random(0f, 400f), random(0f, 400f))
        val pointsAmount = 500_000

        strokeWeight(.3f)

        val start = System.currentTimeMillis()

        repeat(pointsAmount) {
            val percentage = it.toFloat() / pointsAmount * 100f
            if (percentage % 1 == 0.0f) {
                val msSinceStart = System.currentTimeMillis() - start
                val exp = (msSinceStart * 100 / percentage) - msSinceStart

                println("$percentage% (remaining: ${exp / 1000}s)")
            }

            val vectorTarget = vectors[random(vectors.size.toFloat()).toInt()]
            startVector = middleVector(startVector, vectorTarget)

            val dr = distance(startVector, vectors[0])
            val r = linearColor(dr)
            val dg = distance(startVector, vectors[1])
            val g = linearColor(dg)
            val db = distance(startVector, vectors[2])
            val b = linearColor(db)

            stroke(r, g, b)

            point(startVector.x, startVector.y)
        }
    }

    private fun distance(a: Vector, b: Vector): Float {
        val x = a.x - b.x
        val y = a.y - b.y
        val z = a.z - b.z

        return sqrt(x * x + y * y + z * z)
    }

    private fun middleVector(start: Vector, end: Vector): Vector {
        return Vector(
            x = start.x + 0.5f * (end.x - start.x),
            y = start.y + 0.5f * (end.y - start.y),
            z = start.z + 0.5f * (end.z - start.z)
        )
    }

    private fun linearColor(d: Float): Float = -255f / (600f * scale) * d + 255f

    companion object {
        fun run() {
            StrangeAttractorSample().runSketch()
        }
    }
}

fun main() = StrangeAttractorSample.run()
